 
 console.log("working");
  var canvas=document.getElementById('sketch');
  //var p  = document.getElementById('pink');
  var ctx = canvas.getContext('2d');
  // var box = document.getElementsByClassName('nav2');
  // var child = box.getElementsByTagName('div');
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  var c = false;
  var isDrawing=false;
  var isTexting;
  var lastX = 0;
  var lastY = 0;
  var hue = 0;
  var direction = true;
  var strokeline=true;
  var strokeEraser=false;
  var strokerainbow=false;
  var strokeSpray=false;
  var strokePixel=true;
  var strokeRec=false;
  var strokeCir=false;
  var strokeTri=false;
  var currentStroke=[];
  var currentColor=[];
  var currentStrokeType=[];
  var saveModal = document.getElementById('save');
  var closeModal = document.getElementById('close');
  var archiveButton = document.getElementById('archive-button')
  var infoButton = document.getElementById('info-button');
  var infoModal = document.getElementById('info');
  var closeInfo= document.getElementById('close-info');
  var density=50;
  var archiveMessage=false;
  var cPushArray = new Array();
  var cStep = -1;
  var curX = 0, curY = 0;
  var currentShapeObj = null;

  currentStrokeType[0]='strokeline';
  currentStroke[0]=16;//default stroke weight
  currentColor[0]='#000000';//default stroke color
  currentColor[1]='#ffffff';

//firebase config
  var config = {
    apiKey: "AIzaSyA6PLyNXFj6jaAvhjDBl3WmyIz6UXzthM0",
    authDomain: "sketch-app-7f2f9.firebaseapp.com",
    databaseURL: "https://sketch-app-7f2f9.firebaseio.com",
    projectId: "sketch-app-7f2f9",
    storageBucket: "sketch-app-7f2f9.appspot.com",
    messagingSenderId: "307660214101"
  };
  firebase.initializeApp(config);
  console.log(firebase);

  var database=firebase.database();
  var num=0;
  var ref = database.ref('images');

    function getRandomInt(min,max){
      return ~~(Math.random()*(max-min+1))+min;
    }

    function setStroketype() {
      if(currentStrokeType[currentStrokeType.length-1]=='strokeline'){
        setLine();
      }
      else if (currentStrokeType[currentStrokeType.length-1]=='strokeeraser') {
        setEraser();
      }
      else if (currentStrokeType[currentStrokeType.length-1]=='strokespray') {
        setSpray();
      }
      else if (currentStrokeType[currentStrokeType.length-1]=='strokerainbow'){
        setRainbow();
      }
      else if (currentStrokeType[currentStrokeType.length-1]=='strokepixel'){
        setPixel();
      }
      else if (currentStrokeType[currentStrokeType.length-1]=='strokerec'){
        setRec();
      }
      else if (currentStrokeType[currentStrokeType.length-1]=='strokecir'){
        setCir();
      }
    }

    function setRainbow() {
      currentStrokeType.push('strokerainbow');
      strokerainbow=true;
      strokeline=false;
      strokeSpray=false;
      strokePixel=false;
      strokeEraser=false;
      strokeRec=false;
      strokeCir=false;
      ctx.lineWidth = 100;
      hasInput = true;
      
      document.body.style.cursor="url('./images/rainbow2.png'), auto";
    }

    function setLine() {
      currentStrokeType.push('strokeline');
      strokerainbow=false;
      strokeline=true;
      strokeSpray=false;
      strokePixel=false;
      strokeEraser=false;
      strokeRec=false;
      strokeCir=false;
      hasInput = true;
      
      document.body.style.cursor="url('./images/pen.png'), auto";
    }

    function setEraser() {
      currentStrokeType.push('strokeeraser');
      strokerainbow=false;
      strokeline=false;
      strokeSpray=false;
      strokePixel=false;
      strokeEraser=true;
      strokeRec=false;
      strokeCir=false;
      hasInput = true;
      
      document.body.style.cursor="url('./images/eraser2.png'), auto";
    }

    function setSpray() {
      currentStrokeType.push('strokespray');
      strokerainbow=false;
      strokeline=false;
      strokeSpray=true;
      strokePixel=false;
      strokeEraser=false;
      strokeRec=false;
      strokeCir=false;
      hasInput = true;
      
      document.body.style.cursor="url('./images/spray2.png'), auto";
    }

    function setPixel() {
      currentStrokeType.push('strokepixel');
      strokerainbow=false;
      strokeline=false;
      strokeSpray=false;
      strokePixel=true;
      strokeEraser=false;
      strokeRec=false;
      strokeCir=false;
      hasInput = true;

      document.body.style.cursor="url('./images/spray2.png'), auto";
    }

    function setRec() {
      currentStrokeType.push('strokerec');
      strokerainbow=false;
      strokeline=false;
      strokeSpray=false;
      strokePixel=false;
      strokeEraser=false;    
      strokeRec=true;
      strokeCir=false;
      hasInput = true;
  
      document.body.style.cursor="url('./images/rect2.png'), auto";
    }

    function setCir() {
      currentStrokeType.push('strokerec');
      strokerainbow=false;
      strokeline=false;
      strokeSpray=false;
      strokePixel=false;
      strokeEraser=false;    
      strokeRec=false;
      strokeCir=true;
      ctx.beginPath();
      hasInput = true;
 
      document.body.style.cursor="url('./images/circle2.png'), auto";

      if (currentShapeObj != null) {
        currentShapeObj.draw(ctx);
      }
      currentShape = "circle";
      canvas.style.display = 'block';
      
    }


    function setTri() {
      currentStrokeType.push('stroketri');
      strokerainbow=false;
      strokeline=false;
      strokeSpray=false;
      strokePixel=false;
      strokeEraser=false;    
      strokeRec=false;
      strokeCir=false;
      strokeTri=true;
      hasInput = true;
      
      document.body.style.cursor="url('./images/triangle2.png'), auto";
    }

    function deleteCanvas(event) {
      var canvas = document.getElementById('sketch'),
      ctx = canvas.getContext("2d");
      //ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.fillStyle="rgba(255,255,255,1)";
      ctx.fillRect(0, 0, canvas.width, canvas.height);
      cPush();
    }

    function downloadImage(){
      document.getElementById("downloader").download = "image.png";
      document.getElementById("downloader").href = document.getElementById("sketch").toDataURL("image/png").replace(/^data:image\/[^;]/, 'data:application/octet-stream');
      console.log();

    }

    function saveImage() {
      console.log(document.getElementById("sketch").toDataURL("image/png").replace(/^data:image\/[^;]/, 'data:application/octet-stream'));
      var data={
        image:document.getElementById("sketch").toDataURL("image/png").replace(/^data:image\/[^;]/, 'data:application/octet-stream'),
        number:num
      }
      ref.push(data);
      saveModal.classList.remove('save-close');
      saveModal.className += " save-open";
      num++;

    }

    infoButton.onmousedown=function () {
      infoModal.classList.remove('info-close');
      infoModal.className+=' info-open';
      DrawImage();
    }

    archiveButton.onmousedown=function () {
      window.location = "https://pizza3.github.io/Sketch-App/archive.html";
    }

    closeModal.onmousedown=function() {
      saveModal.classList.remove('save-open');
      saveModal.className += " save-close";
    }
    closeInfo.onmousedown=function () {
      infoModal.classList.remove('info-open');
      infoModal.className+=' info-close';
    }

    // if(archiveMessage){
    //   saveModal.classList.remove('save-close');
    //   saveModal.className += " save-open";
    //   console.log('save-open');
    // }
    // else {
    //   saveModal.className += " save-close";
    // }


    function getStroke() {
      // var s=stroke.getAttribute('data-stroke');
      currentStroke.push(document.getElementById("strokewidth").value);
    }

    function getColor() {
      // var h1=color.getAttribute("data-h");
      // var s1=color.getAttribute("data-s");
      // var l1=color.getAttribute("data-l");
      // currentColor.push('hsl('+h1+','+s1+'%,'+l1+'%)');
      currentColor.push('#'+document.getElementById("pick-color").value);
      // for(var i=0;i<child.length;i++){
      //   child[i].style.backgroundColor=ctx.strokeStyle;
      // }
    }

    setInterval(()=>{
      if(document.getElementById("pick-color").value!=currentColor[currentColor.length-1]){
      getColor();
    }},100);

    setInterval(()=>{
      if(document.getElementById("strokewidth").value!=currentStroke[currentStroke.length-1]){
      getStroke();
    }},100);

  canvas.onmousedown = function(e) {

    isDrawing=true;
    
    if(strokeline){
      //ctx.strokeStyle='hsl(0, 0%, 13%)';
      ctx.strokeStyle=currentColor[currentColor.length-1];
      ctx.lineWidth=currentStroke[currentStroke.length-1];//have 8 as default
      ctx.shadowBlur=10;
      ctx.lineJoin = 'round';
      ctx.lineCap = 'round';
      ctx.beginPath();

    }
    else if(strokeEraser){
      ctx.strokeStyle=currentColor[1];
      ctx.lineWidth=currentStroke[currentStroke.length-1];//have 8 as default
      ctx.shadowBlur=10;
      ctx.lineJoin = 'round';
      ctx.lineCap = 'round';
      ctx.beginPath();
    }
    else if (strokerainbow) {
      [lastX, lastY] = [e.offsetX, e.offsetY];
      ctx.lineWidth=currentStroke[currentStroke.length-1];//have 8 as default
      ctx.strokeStyle = '#BADA55'; //ctx is the canvas
	    ctx.lineJoin = 'round';
	    ctx.lineCap = 'round';
    }
    else if (strokeSpray) {
      ctx.strokeStyle=currentColor[currentColor.length-1];
      ctx.fillStyle=ctx.strokeStyle;
      ctx.lineWidth=currentStroke[currentStroke.length-1]/2;
      ctx.lineJoin=ctx.lineCap='round';
    }
    else if (strokePixel) {
      ctx.strokeStyle=currentColor[currentColor.length-1];
    }
    else if(strokeRec){
      ctx.strokeStyle=currentColor[currentColor.length-1];
      ctx.lineWidth=currentStroke[currentStroke.length-1];//have 8 as default
      ctx.shadowBlur=10;
      ctx.lineJoin = 'round';
      ctx.lineCap = 'round';
      ctx.beginPath();
    }
    else if(strokeCir){
      /*
      curX = e.clientX - canvas.offsetLeft;
      curY = e.clientY - canvas.offsetTop;
      flagShape = true;

      canvas.style.display = 'block';
      createShape(new Point(curX, curY));*/
      ctx.strokeStyle=currentColor[currentColor.length-1];
      ctx.lineWidth=currentStroke[currentStroke.length-1];//have 8 as default
      ctx.shadowBlur=10;
      ctx.lineJoin = 'round';
      ctx.lineCap = 'round';
      ctx.beginPath();
    }
    else if(strokeTri){
      ctx.strokeStyle=currentColor[currentColor.length-1];
      ctx.lineWidth=currentStroke[currentStroke.length-1];//have 8 as default
      ctx.shadowBlur=10;
      ctx.lineJoin = 'round';
      ctx.lineCap = 'round';
      ctx.beginPath();
    }
    //ctx.moveTo(e.clientX,e.clientY);
    //cPush();
  };

  canvas.touchstart = function(e) {
    isDrawing=true;
    ctx.shadowBlur=10;
    ctx.lineJoin ='round';
    ctx.lineCap ='round';
    ctx.beginPath();
    //ctx.moveTo(e.clientX,e.clientY);
  };

  //part where the magic happens
  canvas.onmousemove = function(e) {
    if(isDrawing){
      //ctx.beginPath();
      if (strokeline) {
        ctx.lineTo(e.clientX-50,e.clientY-50);
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(e.clientX-50, e.clientY-50,   ctx.lineWidth*2, 0, Math.PI*2);
        ctx.fillStyle='hsl(0, 0%, 13%)';
        ctx.beginPath();
        ctx.moveTo(e.clientX-50,e.clientY-50);
        //cPush();
      }
      else if(strokeEraser){
        ctx.lineTo(e.clientX-50,e.clientY-50);
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(e.clientX-50, e.clientY-50,   ctx.lineWidth*2, 0, Math.PI*2);
        ctx.fillStyle='hsl(0, 0%, 13%)';
        ctx.beginPath();
        ctx.moveTo(e.clientX-50,e.clientY-50);
      }
      else if (strokerainbow) {
        ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
        ctx.beginPath();
      // start from
        ctx.moveTo(lastX, lastY);
      // go to
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
        [lastX, lastY] = [e.offsetX, e.offsetY];
        hue++;
        if (hue >= 360) {
          hue = 0;
        }
      }
      else if (strokeSpray) {
        for(var i=density;i--;){
        var radius=20;
        var offsetradius=getRandomInt(-ctx.lineWidth,ctx.lineWidth);
        var offsetangle=getRandomInt(0,360);
        ctx.fillRect(e.clientX-50 + offsetradius*Math.cos(offsetangle),e.clientY-50 + offsetradius*Math.sin(offsetangle), 1, 1);
      }
    }
    else if (strokePixel) {
      for(var i=0;i<currentStroke[currentStroke.length-1];i+=currentStroke[currentStroke.length-1]/10){
        for(var j=0;j<currentStroke[currentStroke.length-1];j+=currentStroke[currentStroke.length-1]/10){
          if(Math.random()>0.7){
            var hue1=getRandomInt(0,360);
            ctx.fillStyle = `hsl(${hue1}, ${hue1}%, 50%)`;
            ctx.fillRect(e.clientX+i-50-currentStroke[currentStroke.length-1]/2,e.clientY+j-50-currentStroke[currentStroke.length-1]/2,currentStroke[currentStroke.length-1]/10,currentStroke[currentStroke.length-1]/10);
          }
        }
      }
    }
    else if(strokeRec) {
      ctx.rect(e.clientX-50,e.clientY-50, 100, 100);
      ctx.stroke();
      ctx.beginPath();
      ctx.arc(e.clientX-50, e.clientY-50,   ctx.lineWidth*2, 0, Math.PI*2);
      ctx.fillStyle='hsl(0, 0%, 13%)';
      ctx.beginPath();
      ctx.moveTo(e.clientX-50,e.clientY-50);
    }
    else if(strokeCir) {
      /*
      curX = e.clientX - canvas.offsetLeft;
      curY = e.clientY - canvas.offsetTop + 18;//height of icon
    
      if (flagShape){
        ctx.clearRect(0, 0, w, h);
        ctx.beginPath();
        if (currentShape == 'curve')
        {
          currentShapeObj.setMidPoint(new Point(curX,curY));
        }else
          currentShapeObj.setLastpoint(new Point(curX, curY));
        currentShapeObj.draw(ctx);
      }*/
      ctx.arc(e.clientX-50,e.clientY-50, 50, 0, Math.PI*2, false);
      ctx.stroke();
      ctx.beginPath();
      ctx.arc(e.clientX-50, e.clientY-50,   ctx.lineWidth*2, 0, Math.PI*2);
      ctx.fillStyle='hsl(0, 0%, 13%)';
      ctx.beginPath();
      ctx.moveTo(e.clientX-50,e.clientY-50);
    }
    else if(strokeTri) {
      
      ctx.lineTo(e.offsetX, e.offsetY+100);
      ctx.lineTo(e.offsetX+100, e.offsetY);
      ctx.lineTo(e.offsetX, e.offsetY);
      ctx.stroke();
      ctx.beginPath();
      ctx.arc(e.clientX-50, e.clientY-50,   ctx.lineWidth*2, 0, Math.PI*2);
      ctx.fillStyle='hsl(0, 0%, 13%)';
      ctx.beginPath();
      ctx.moveTo(e.clientX-50,e.clientY-50);
      
    }
  }
};

  canvas.touchmove = function(e) {
    if(isDrawing){
      //ctx.beginPath();
      ctx.lineTo(e.clientX,e.clientY);
      ctx.stroke();
      ctx.beginPath();
      ctx.arc(e.clientX, e.clientY, ctx.lineWidth*2, 0, Math.PI*2);
      ctx.fillStyle='hsl(0, 0%, 13%)';
      ctx.beginPath();
      ctx.moveTo(e.clientX,e.clientY);

    }
  };

  canvas.onmouseup = function(){
    cPush();
    isDrawing=false;
    canvas.getContext('2d').saveHistory();
    ctx.closePath();
  };

  canvas.touchend = function(){
    isDrawing=false;
  };

  canvas.onmouseleave = function(){
    isDrawing=false;
  };
  
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('sketch').toDataURL());
}

function cUndo() {
  document.body.style.cursor="url('./images/undo2.png'), auto";
  if (cStep > 0) {
      cStep--;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

function cRedo() {
  document.body.style.cursor="url('./images/redo2.png'), auto";
  if (cStep < cPushArray.length-1) {
      cStep++;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}


function invertColor() {
  var pixels=ctx.getImageData(0, 0, canvas.width, canvas.height);
  var data=pixels.data;
  for(var i=0;i<data.length;i+=4){
    data[i]=255-data[i];
    data[i+1]=255-data[i+1];
    data[i+2]=255-data[i+2];
  }
  ctx.putImageData(pixels, 0, 0);
}

function loadFile(input) {
  var file=input.files[0];
  var src=URL.createObjectURL(file);
  var img=new Image();
  img.src=src;
  img.onload=function(){
    ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
    cPush();
  };
}
/*
window.onload=function(){
  document.body.style.cursor="cell"
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.beginPath();
  ctx.fillStyle="rgba(255,255,255,1)";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  cPush();
}*/

function init(){
  ctx.fillStyle="rgba(255,255,255,1)";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  cPush();
}
///////////////////////////////////////


    var font = '14px sans-serif';
    var hasInput = false;
    var font_name = 'Arial';
    var font_size=2;

function  text(){

  document.body.style.cursor="url('./images/text2.png'), auto";
  hasInput=false;
  canvas.onclick = function(e) {
    if (hasInput) return;
    addInput(e.clientX, e.clientY);
  
  }
}

function addInput(x, y) {
  
    var input = document.createElement('input');
    
    font = 8*font_size+'px '+font_name;
    input.type = 'text';
    input.style.position = 'static';
    input.style.left = (x) + 'px';
    input.style.top = (y) + 'px';
    input.style.font = font;
    input.style.color = currentColor[currentColor.length-1];

    input.onkeydown = handleEnter;

    document.body.appendChild(input);
   
    input.focus();
    
    hasInput = true;
}

function handleEnter(e) {
    var keyCode = e.keyCode;
    
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        hasInput = false;
    }
}

function drawText(txt, x, y) {
    ctx.fillStyle = currentColor[currentColor.length-1];
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.font = font;
    ctx.fillText(txt, x-45, y-45);
    
    cPush();
}

function setFont(f) {
  if (f == 0) {
    font_name = 'Arial';
  } else if (f == 1) {
    font_name = 'Verdana';
  } else if (f == 2) {
    font_name = 'Courier New';
  } else if (f == 3) {
    font_name = 'serif';
  } else if (f == 4) {
    font_name = 'sans-serif';
  } else if (f == 5) {
    font_name = '微軟正黑體';
  } else if (f == 6) {
    font_name = '新細明體';
  } else if (f == 7) {
    font_name = '標楷體';
  }
}

function setFont_size(value) {
  font_size = Math.round(parseInt(value) / 10);
  if (font_size == 0)
    font_size = 1;
}