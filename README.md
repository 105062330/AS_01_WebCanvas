# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

Report:
    這次的作業要我們用html canvas做一個小畫家，所以我在body裡放了一個canvas標籤，id="sketch"，在javascript裡設一個變數canvas＝document.getElementBy('sketch')再設一個ctx=canvas.getContext('2d')，代表在canvas上要畫2d圖形．首先我設一個boolean strokeline=true，其他的stroke設為false, 再設一個currentStrokeType=[]來存現在要描繪的物體，currentColor=[]來存目前選的顏色，currentStroke來存目前畫筆的寬度，之後把currentStrokeType[0]設為strokeline也就是default．之後在canvas.mousedown裡if strokeline=true，我先把顏色調為目前選的顏色，ctx.strokeStyle設成currentColor[currentColor.length-1]，再設寬度ctx.lineWidth=currentStroke[currentStroke.length-1]，設完之後ctx.beginPath()就開始畫，另外因為畫線會有稜角，所以我把ctx.lineJoin和ctx.lineCap設為'round'，線畫起來就比較圓滑一些．Mousedown之後就開始mousemove=function(e)，用ctx.lineTo(e.clientX-50,e.clientY-50)取e的x和y，之後再ctx.stroke()在畫布上面畫出來，基本的畫線就出來了．

        Color:
            我先在html放一個input，設他的id='pick-color'之後利用document.getElementById().value將選到的顏色push上currentColor即可．

        Width:
            跟顏色一樣，在html放一個input，id='strokewidth'後，利用document.getElementById("strokewidth"將選到的畫筆寬度push上currentStroke即可．

        Line：
            除了default，之後也會有可能在別的工具切換回畫線，所以我設了一個div，只要onclick就會跑setLine()，在setLine()裡我把currentStrokeType push了
            strokeline，然後把boolean strokeline設為true，其他的boolean設為false，其他的步驟就跟default一樣，不在贅述．

        Eraser:
            eraser做法跟線條一樣，唯一不同的是我將currentColor[1]設為白色，所以ctx.strokeStyle設為currentColor[1]的畫畫筆就會是白色，畫出來的感覺就會像是橡皮擦．

                                        -------Other useful widgets-------
        Rainbow：
            一樣在彩虹的div上onclick就會跑setRainbow()，在setRainbow()裡將currentStroke push strokerainbow，再把彩虹的boolean設為true，其他的設為false．跟畫線不一樣的地方是，我另外設一個變數hue來改變彩虹的顏色，再mousedown裡ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`，hue每次就加1，加到360後在歸零不斷的循環，這樣畫出來的線就會去彩紅的感覺．

        Spray:
            在準備spray之前，我先寫一個可以取亂數的function getRandomInt()傳入max和min後直接return ~~(Math.random()*(max-min+1))+min．之後我先設2個變數offsetradius=getRandomInt(-ctx.lineWidth,ctx.lineWidth), offsetangle=getRandomInt(0,360)，兩個變數都隨機取數字，然後ctx.fillRect(e.clientX-50 + offsetradius*Math.cos(offsetangle),e.clientY-50 + offsetradius*Math.sin(offsetangle), 1, 1)，在ctx畫一個正方形，因為是前面兩個變數是隨機的從負到正，也有可能是0，後面的角度角度取完三角函數後成前面的亂數，所以有些畫不出來，全部畫起來就會有噴霧的感覺．

        Pixel:
            pixel的方式則有點像spray和彩虹的結合版，它的顏色是用設一個變數hue1=getRandomInt(0,360)，一樣是隨機取數字，ctx.fillStyle在設成`hsl(${hue1}, ${hue1}%, 50%)`，這樣做就會有隨機的顏色．至於形狀，ctx.fillRect(e.clientX+i-50-currentStroke[currentStroke.length-1]/2,e.clientY+j-50-currentStroke[currentStroke.length-1]/2,currentStroke[currentStroke.length-1]/10,currentStroke[currentStroke.length-1]/10)，外面再包兩個for迴圈控制i和j，就會有許多亂數小正方形畫出來，再配上隨機的顏色，畫出來就會是pixel．

        Invertcolor：
            在invertcolor的div上onclick就會跑invertColor()，在function裡設一個pixel=ctx.getImageData(0, 0, canvas.width, canvas.height)，就是取整個canvas的圖，然後設data=pixel.value取pixel的值，然後設一個迴圈for(var i=0;i<data.length;i+=4)，因為1個pixel是4bits所以i要+=4，之後在迴圈裡data[i]=255-data[i]，data[i+1]=255-data[i+1]，data[i+2]=255-data[i+2]，就會反轉整個畫面的顏色，之後再ctx.putImageData(pixels, 0, 0)利用putImageData這個function畫上去即可．

                                        -------Other useful widgets-------
    
        Shape：
            我3個圖形放在一起講，因為作法實在是大同小異，其實取寬度和顏色的方法也是跟線條的方法一樣，跟線條不同的是，線條是用ctx.lineTo()，正方形是用ctx.rect()，圓形是用ctx.arc()，而三角形是用3個lineTo()，將三條線接在一起就會跑出三角形的圖案，不過這樣會不斷的跑出圖形，沒辦法用拉的出現一個圖形並且決定大小．關於另一種方法我原本又設了一個canvas來暫存中間不斷跑出的圖形，mouseup之後在原本的canvas再把決定的圖形畫出來，不過試到最後還是沒辦法出現，再加上時間的問題，之後有時間再深入研究．

        Undo, Redo:
            做undo, redo我的做法是用每次畫完圖就存一次圖，做undo的時候就回上一張圖，redo就是下一張圖．我先設cPushArray和cStep，cPushArray是用來存圖片，cStep是cPushArray的index．我總共用了3個function，除了cUndo()，cRedo()外還有一個cPush()，cPush()就是用來存圖然後再push上cPushArray裡．一開始cStep設-1，所以每次cPush()時cStep會+1，然後cPushArray.push(document.getElementById('sketch').toDataURL())來存canvas上的圖片，然後跑cUndo()的時候如果cStep>0，cStep就-1，再把cPushArray[cStep]畫上去．畫上去的方法就是設變數canvasPic = new Image()，src就是cPushArray[cStep]，之後主動call canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0)即可將照片畫上去看起來就像是上一步．至於cRedo()則是 if cStep < cPushArray.length-1，cStep就+1，畫上去的方法就跟cUndo()一樣．cPush()的場合一開始在window.onload就會先push一次，之後在canvas.onmouseup和打完字後個會push一次．在做undo，redo的時候我一開始只有橡皮擦可以作用，之後才發現是因為canvas一開始是透明的，所以就算透明的圖片畫上去還是不會有改變，解決方法就是在body onload的時候在canvas畫一個白色的方形，放上三行ctx.beginPath();ctx.fillStyle="rgba(255,255,255,1)";ctx.fillRect(0, 0, canvas.width, canvas.height);即可解決．

        Text:
            text則比較麻煩，需要4個function．我先設一個boolean hasInput一開始設為false，第一個function是text()，onclick時會跑進去，跑進去之後canvas上onclick後會跑到第二個function addInput()，裡面需要用到document.createElement()，設一個變數input = document.createElement('input')，並且將input.type 設為'text';就可以在canvas.onclick後輸入文字，之後按enter後再使用document.body.appendChild()，function給他input後即可將字畫在畫布上．至於第三個function是在鍵盤發生事件後跑到handleEnter(e)，在handleEnter(e)裡如果keyCode === 13也就是按下enter會跑到第4個function drawText()，也就是把字畫在canvas上，在drawText()裡主要用ctx.fillText()，即可畫上去，至於其他的字體或顏色在下面說明．
    
        Text.font:
            字體的選擇我在html裡放了select標籤，只要onchange就會跑setFont()，就會把字體調成對應選擇的值，然後在font裡放入8*font_size+'px '+font_name後，最後再在上面的drawText()裡將ctx.font設成font即可．

        Text.size:
            字體的大小我則是放了input標籤type="range"，一樣只要onchange就會跑setFont_size()，就會將font_size調成input的值，最後在font裡一樣放入8*font_size+'px '+font_name，在上面的drawText()裡將ctx.font設成font即可．

        Text.bug:
            因為上面提到的undo問題，我必須將canvas畫成白的，以至於我無法再輸入text的時候看到輸入的框框和打的字，不過似乎會跟著瀏覽器的不同而有所改變，像是在IE edge就會有輸入的指標出現，不過在google chrome上則是什麼都看不到，這個問題目前也還沒想到別種方法解決．

        Upload photo:
            上傳圖片首先在html放一個input type="file"，只要onchage就會跑到loadFile()，在function裡設了一個變數file=input.files[0]來存取input的物件，再設一個src利用createObjectURL()取得file的url後，用內建的Image()和ctx.drawImage()將圖片畫到canvas上．

        Download:
            在download的圖案上onclick就會跑到downloadImage()，首先先命名檔名叫image.png，之後再利用document.getElementById().href和document.getElementById().toDataURL().replace將檔案下載下來就好．
        
        Clear:
            在clear的圖案上onclick會跑到deleteCanvas()，然後在function裡面跟undo，redo一樣利用ctx.fillRect()在canvas上畫一個白色的正方形即可．

        Cursor:
            cursor的部分我是在每個要改變的地方只要onclick就放document.body.style.cursor=url，url再放想要的圖案即可，中間碰到不管放什麼圖案都跑不出來，只要將圖案縮小到合適的尺寸就好．